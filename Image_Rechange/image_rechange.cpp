#include "image_rechange.h"

Image_Rechange::Image_Rechange(QWidget *parent)
: QMainWindow(parent)
{
	ui.setupUi(this);
	nowSum = 0;
	flag_doc = 0;//0表示用户点击的是打开文件；1表示用户点击的是打开文件夹
	if (ui.checkBox_staylonghigh->isChecked()){
		ui.comboBox->setEnabled(true);
	}
	QCheckBox *button = ui.checkBox_staylonghigh;
	
	QCheckBox *buttonQ = ui.checkBox_quality;
	QCheckBox *buttonS = ui.checkBox_speed;
	connect(button, SIGNAL(clicked()), this, SLOT(on_checkbox_clicked()));
	connect(buttonQ, SIGNAL(clicked()), this, SLOT(on_checkbox_quality_clicked()));
	connect(buttonS, SIGNAL(clicked()), this, SLOT(on_checkbox_speed_clicked()));
}

Image_Rechange::~Image_Rechange()
{

}

//打开文件
void Image_Rechange::on_pushButton_openDoc_clicked(){
	int i = 0;

	//定义文件对话框类
	QFileDialog *fileDialog = new QFileDialog(this);
	//定义文件对话框标题
	fileDialog->setWindowTitle("Open Files");
	//设置默认文件路径
	fileDialog->setDirectory(".");
	////设置文件过滤器
	//fileDialog->setNameFilter(tr("Images(*.png *.jpg *.jpeg *.bmp)"));
	//设置可以选择多个文件,默认为只能选择一个文件QFileDialog::ExistingFiles
	fileDialog->setFileMode(QFileDialog::ExistingFiles);
	//设置视图模式
	fileDialog->setViewMode(QFileDialog::Detail);
	//打印所有选择的文件的路径

	if (fileDialog->exec())
	{
		fileNames = fileDialog->selectedFiles();
	}

	while (i < fileNames.size())
	{
		ui.textBrowser->append(fileNames[i]);
		i++;
	}
	//要修改的文件总数
	numSum = fileNames.size();
	flag_doc = 0;
}

//打开文件夹
void Image_Rechange::on_pushButton_openDocs_clicked(){

	//文件夹路径
	srcDirPathChange = QFileDialog::getExistingDirectory(
		this, "choose save Directory",
		"/");

	if (srcDirPathChange.isEmpty())
	{
		return;
	}
	else
	{
		ui.textBrowser->append(QString(srcDirPathChange));
		srcDirPathChange += "/";
	}
	//判断路径是否存在
	QDir dir(srcDirPathChange);
	if (!dir.exists())
	{
		return;
	}

	//获取所选文件类型过滤器
	QStringList filters;
	filters << QString("*.jpeg") << QString("*.jpg") << QString("*.png") << QString("*.tif") << QString("*.gif") << QString("*.bmp");

	//定义迭代器并设置过滤器
	QDirIterator dir_iterator(srcDirPathChange, filters, QDir::Files | QDir::NoSymLinks, QDirIterator::Subdirectories);
	while (dir_iterator.hasNext())
	{
		dir_iterator.next();
		QFileInfo file_info = dir_iterator.fileInfo();
		QString absolute_file_path = file_info.absoluteFilePath();
		ui.textBrowser->append(absolute_file_path);
		/*output << absolute_file_path.toStdString() << std::endl;*/
		fileNames.append(absolute_file_path);
	}
	numSum = fileNames.size();
	flag_doc = 1;
}

//点击按dpi修改按钮事件
void Image_Rechange::on_pushButton_changeDPI_clicked(){
	QImage img;
	QString imgfile, srcDirPath, targetPath, targetPathLeft;
	int dpi;
	QString n_point = "/";
	int m_index;
	int i = 0;
	dpi = ui.lineEdit_dpi->text().toInt();
	//打开保存文件夹路径
	srcDirPath = QFileDialog::getExistingDirectory(
		this, "choose save Directory",
		"/");

	if (srcDirPath.isEmpty())
	{
		return;
	}
	else
	{
		ui.textBrowser->append(QString(srcDirPath));
		srcDirPath += "/";
	}


	while (i < fileNames.size())
	{
		imgfile = fileNames[i];
		m_index = imgfile.lastIndexOf(n_point);
		img.load(imgfile);
		img.setDotsPerMeterX(dpi * 39.37);
		img.setDotsPerMeterY(dpi * 39.37);
		if ((ui.checkBox_staydir->isChecked()) && (flag_doc))
		{
			targetPath = imgfile.replace(srcDirPathChange, srcDirPath);
		}
		else{
			targetPath = imgfile.replace(0, m_index+1 , srcDirPath);
		}
		m_index = targetPath.lastIndexOf(n_point);
		targetPathLeft = targetPath.left(m_index );
		QDir dir(targetPathLeft);
		if (!dir.exists()) {
			dir.mkpath(targetPathLeft);  // 创建路径  
		}
		img.save(targetPath);
		nowSum = i;
		startProgress(nowSum);
		i++;
	}
}

//点击按分辨率修改按钮事件
void Image_Rechange::on_pushButton_changeFBL_clicked(){
	QImage img;
	QString imgfile, srcDirPath, targetPath, targetPathLeft;
	int result, m_long, m_high;
	QString n_point = "/";
	int m_index;
	int i = 0;
	m_long = ui.lineEdit_long->text().toInt();
	m_high = ui.lineEdit_high->text().toInt();
	int intc = ui.comboBox->count();

	//打开保存文件夹路径
	srcDirPath = QFileDialog::getExistingDirectory(
		this, "choose save Directory",
		"/");

	if (srcDirPath.isEmpty())
	{
		return;
	}
	else
	{
		ui.textBrowser->append(QString(srcDirPath));
		srcDirPath += "/";
	}

	if (ui.checkBox_staylonghigh->isChecked())
	{
		while (i < fileNames.size())
		{
			imgfile = fileNames[i];
			m_index = imgfile.lastIndexOf(n_point);
			img.load(imgfile);
			if (intc==1)
			{
				img = img.scaled(m_long, m_high, Qt::KeepAspectRatio, Qt::SmoothTransformation);
			}
			else if (intc==2)
			{
				img = img.scaled(m_long, m_high, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
			}
			else
			{
				img = img.scaled(m_long, m_high, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation);
			}
			
			if ((ui.checkBox_staydir->isChecked()) && (flag_doc))
			{
				targetPath = imgfile.replace(srcDirPathChange, srcDirPath);
			}
			else{
				targetPath = imgfile.replace(0, m_index + 1, srcDirPath);
				m_index = targetPath.lastIndexOf(n_point);
			}
			targetPathLeft = targetPath.left(m_index );
			QDir dir(targetPathLeft);
			if (!dir.exists()) {
				dir.mkpath(targetPathLeft);  // 创建路径  
			}
			img.save(targetPath);
			nowSum = i;
			startProgress(nowSum);
			i++;
		}
	}
	else{
		while (i < fileNames.size())
		{
			imgfile = fileNames[i];
			m_index = imgfile.lastIndexOf(n_point);
			img.load(imgfile);
			img = img.scaled(m_long, m_high, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
			if ((ui.checkBox_staydir->isChecked()) && (flag_doc))
			{
				targetPath = imgfile.replace(srcDirPathChange, srcDirPath);
			}
			else{
				targetPath = imgfile.replace(0, m_index + 1, srcDirPath);
				m_index = targetPath.lastIndexOf(n_point);
			}
			
			targetPathLeft = targetPath.left(m_index );
			QDir dir(targetPathLeft);
			if (!dir.exists()) {
				dir.mkpath(targetPathLeft);  // 创建路径  
			}
			img.save(targetPath);
			nowSum = i;
			startProgress(nowSum);
			i++;
		}
	}
}

//清空按钮
void Image_Rechange::on_pushButton_clear_clicked(){
	ui.textBrowser->clear();
	fileNames.clear();
}

//进度条更新事件
void Image_Rechange::startProgress(int nowSum){
	int numResult = 0;
	numResult = ((nowSum+1)*1.00 / numSum)*100;
	ui.progressBar->setValue(numResult);
}

//保持长宽比box被点击的事件
void Image_Rechange::on_checkbox_clicked(){
	if (ui.checkBox_staylonghigh->isChecked())
	{
		ui.comboBox->setEnabled(true);

	}
	else
	{
		ui.comboBox->setDisabled(true);
	}
}

//质量优先box被点击的事件
void Image_Rechange::on_checkbox_quality_clicked(){
	if (ui.checkBox_quality->isChecked())
	{
		ui.checkBox_speed->setChecked(false);

	}
	else
	{
		ui.checkBox_speed->setChecked(true);
	}
}
//速度优先box被点击的事件
void Image_Rechange::on_checkbox_speed_clicked(){
	if (ui.checkBox_speed->isChecked())
	{
		ui.checkBox_quality->setChecked(false);

	}
	else
	{
		ui.checkBox_quality->setChecked(true);
	}
}


